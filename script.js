
// Déclaration d'une variable 'active' =
// Liste de noeuds ciblant les balises <li>
//récupépée via la méthode QuerySelectorAll
let active = document.querySelectorAll('li');
// document.querySelectorAll('.key')
//Déclaration d'un constante = objet audio
//=> new Audio
const audio = new Audio();
//Fonction fléchèe qui permet de jouer l'audio 
//Et qui prend la note en paramètre = chemin vers l'audio
const ring = (key) => {
    //Déclaration d'une variable = concaténation du chemin vers l'audio corespondant à la note à jouer.
    let path = "tunes/"+key+".wav";
    // Affectation du chemin à la propriété src de l'objet audio
    audio.src = path;
    // Appel de la méthode play sur l'objet audio => joue la note
    audio.play();
};

// 1. Jouer au piano avec le clavier

//J'écoute sur le DOM(document) un événement , 'keydown' = une frappe sur le clavier via la méthode addEventlistener 
document.addEventListener('keydown',(e)=>{
    //Je récupère la valeur de la frappe via la proprieté key de l'objet e (e.key)
    const press_Key = e.key; 
    let cChange = document.querySelector(`[data-key="${press_Key}"]`);
    // je pose deux conditions
    // 1-la lettre correspond à une des note de la gamme  => je joue corespondant la note > evite les erreurs #
    //2-sinon rien
        if (cChange){
            //j'appelle la fonction 'ring' qui prend en paramètre la lettre correspondant à la note à jouer
            // console.log(e.dataset.key);
            ring(e.key);
            //Ajout de la classe active = CSS pression sur la note
            cChange.classList.add("active");           
        };
        //J'écoute sur le DOM(document) un événement , 'keyup' = laché d'une touche du clavier via la méthode addEventlistener 
    document.addEventListener('keyup',(e)=>{
        // Suppression de la classe active
        cChange.classList.remove("active")  
    });
});

//2.Jouer au Piano avec le clavier

//Bouclage sur la balise li(liste) via la méthode foreach
active.forEach((i)=>{
    //Ecoute d'un événement click
    i.addEventListener("click", (e)=>{
        //Je récupère la valeur de data-key que je déclare dans une variable pour facilité la lecture
        let note = i.dataset.key;
        //Ajout de la classe active = CSS pression sur la note
        i.classList.add("active");
            //Si la note est true, je joue la note => appel de la fonction ring avec en paramètre la valeur de la note(lettre correspondante)
            if (note){
                ring(note);
            }
   });
   //J'écoute l'événement , 'mouseout' = lorsque le pointeur de la souris quitte la note, suppression de la classe active
    i.addEventListener("mouseout", (e)=>{
        i.classList.remove("active");
    });
});


